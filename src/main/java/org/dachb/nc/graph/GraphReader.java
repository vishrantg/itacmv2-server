/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dachb.nc.graph;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections15.Factory;
import org.apache.commons.lang3.StringUtils;
import org.dachb.nc.config.GsonUtility;
import org.dachb.nc.config.GsonUtilityImpl;
import org.dachb.nc.graph.datastructure.Edge;
import org.dachb.nc.graph.datastructure.EdgeDetails;
import org.dachb.nc.graph.datastructure.EdgeFactory;
import org.dachb.nc.graph.datastructure.Vertex;
import org.dachb.nc.graph.datastructure.VertexDetail;
import org.dachb.nc.graph.datastructure.VertexFactory;

import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;

/**
 * Turns a coggle csv file into a graph. The file contains a list of edges in
 * the following format:
 *
 * Promoted to Inside Sales Better Benefits Retain Employee Longer Territory
 * Continuity Customer Loyalty Charger Higher Prices Higher Profits Company
 * Growth Shareholder Growth Research and Development (Innovation) Market
 * Expertise Perception of Market Leader Charge Higher Prices Higher Profits
 * Research and Development (Innovation) Company Growth Shareholder Growth
 * Better Commissions Retain Employee Longer Territory Continuity Customer
 * Loyalty *
 *
 * @author Aqualonne
 */
/**
 *
 * @author Vishrant Gupta
 */
public class GraphReader {

	// Added by Vishrant
	private BetweennessCentrality<Vertex, Edge> betweennessCentrality;

	private boolean debug = false;

	private Factory<Edge> edge_factory;

	private Graph<Vertex, Edge> graph;

	// added by Vishrant
	// @Autowired, as not a spring component
	GsonUtility gson = new GsonUtilityImpl();

	private HashMap<String, Vertex> nameToNode;
	private Factory<Vertex> vertex_factory;

	// added by Vishrant
	public GraphReader() {
		this.graph = new DirectedSparseGraph<Vertex, Edge>();// ObservableGraph<Vertex,Edge>(new

		this.vertex_factory = new VertexFactory();
		this.edge_factory = new EdgeFactory();

		this.nameToNode = new HashMap<String, Vertex>();
	}

	// Added by Vishrant
	public GraphReader(byte[] graphData) throws Exception {

		InputStream is = new ByteArrayInputStream(graphData);
		BufferedReader br = new BufferedReader(new InputStreamReader(is,
				"UTF-8"));

		this.loadMap(br);
	}

	public GraphReader(File f) throws Exception {

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(f));

			this.loadMap(br);
			// } catch (Exception e) {
			// System.err.println("[GraphReader:] Couldn't read (" +
			// e.getMessage() + ")");
			// }
		} finally {
			if (br != null) {
				br.close();
			}
		}
	}

	public void addEdge(String from, String to) {
		Vertex fromV = this.getNode(from);
		Vertex toV = this.getNode(to);
		// Check if there is already an edge
		for (Vertex v : this.graph.getSuccessors(fromV)) {
			if (this.debug) {
				System.out.println("\t\t\t" + v.getFullName());
			}
			if (v.getFullName().equals(to)) {
				return;
			}
		}
		// if the edge HAS to be added, then we add it.
		Edge e = this.edge_factory.create();
		this.graph.addEdge(e, fromV, toV, EdgeType.DIRECTED);
		if (this.debug) {
			System.out.println("<" + e.getId() + ">[" + from + "]->[" + to
					+ "]");
		}
	}

	// A line looks like ",,,,Promoted to Inside Sales,,,,,,,". By couting the
	// number of ',' before the first letter we know how deep the line is.
	private int countSpace(String line) {
		char[] content = line.toCharArray();
		int count = 0;
		while (count < content.length
				&& !StringUtils.isAlpha("" + content[count])) {
			count++;
		}
		return count;
	}

	// Added by Vishrant
	public BetweennessCentrality<Vertex, Edge> getBetweennessCentrality() {
		return this.betweennessCentrality;
	}

	public Graph<Vertex, Edge> getGraph() {
		return this.graph;
	}

	// Added by Vishrant
	public String getGraphInJSONFormat() {

		Collection<Edge> edges = this.graph.getEdges();
		Collection<Vertex> vertices = this.graph.getVertices();

		Collection<VertexDetail> nodeDetails = new ArrayList<VertexDetail>();
		Collection<EdgeDetails> edgeDetails = new ArrayList<EdgeDetails>();

		// TODO remove below random code
		Random randomizer = new Random();
		List<String> list = new ArrayList() {
			{
				this.add("#e8ff1f");
				this.add("#21ff9d");
			}
		};

		for (Vertex vertex : vertices) {
			VertexDetail vertexDetail = new VertexDetail();
			vertexDetail.setId(vertex.getId());
			vertexDetail.setName(vertex.getFullName());
			vertexDetail.setColor(list.get(randomizer.nextInt(list.size())));

			if (this.betweennessCentrality != null) {
				vertexDetail.setScore(this.betweennessCentrality
						.getVertexScore(vertex));
			}

			nodeDetails.add(vertexDetail);
		}

		for (Edge edge : edges) {

			Vertex source = this.graph.getSource(edge);
			Vertex dest = this.graph.getDest(edge);

			EdgeDetails edgeDetail = new EdgeDetails();
			edgeDetail.setSource(source.getFullName());
			edgeDetail.setTarget(dest.getFullName());

			edgeDetail.setId(edge.getId());

			if (this.betweennessCentrality != null) {
				edgeDetail.setScore(this.betweennessCentrality
						.getEdgeScore(edge));
			}

			edgeDetails.add(edgeDetail);
		}

		org.dachb.nc.graph.datastructure.Graph graphDS = new org.dachb.nc.graph.datastructure.Graph();
		graphDS.setEdges(edgeDetails);
		graphDS.setNodes(nodeDetails);

		String graphData = this.gson.toJson(graphDS);

		return graphData;
	}

	private Vertex getNode(String name) {
		Vertex v = null;
		if (!this.nameToNode.containsKey(name)) {
			v = this.vertex_factory.create();
			v.setFullName(name);
			this.graph.addVertex(v);
			this.nameToNode.put(name, v);
		} else {
			v = this.nameToNode.get(name);
		}
		return v;
	}

	public void loadMap(BufferedReader br) throws IOException {
		this.graph = new DirectedSparseGraph<Vertex, Edge>();// ObservableGraph<Vertex,Edge>(new

		this.vertex_factory = new VertexFactory();
		this.edge_factory = new EdgeFactory();

		this.nameToNode = new HashMap<String, Vertex>();

		List<String> lastNodePerLevel = new LinkedList<String>();// stores the
		// last
		// factor
		// seen per
		// each line
		// level,
		// i.e.
		// lastNodePerLevel[2]
		// is the
		// name of
		// the
		// factor
		// occupying
		// level 2
		int previousLine = 0;// count of how deep the previous line was, so we
		// know whether we're creating links or back
		// tracking
		// the first line is a special case because we can't create an edge, so
		// we just make the vertex

		// Added by Vishrant: Fix to ignore empty nodes
		String firstVertex = (br.readLine().toLowerCase()).replace(",", "");
		this.getNode(firstVertex);
		lastNodePerLevel.add(firstVertex);
		String line = "";
		// starts processing all other lines
		while (true) {
			line = br.readLine();
			if (line == null
					|| line.replace("\n", "").replace("\r", "").isEmpty()) {
				break;
			}
			/**
			 * *******************Processes a line**********************
			 */
			line = line.toLowerCase();
			int currentLine = this.countSpace(line);
			line = line.replace(",", "");// removes the , so we get just the
			// name
			line = line.replaceAll("\n", "").replaceAll("\r", "");

			if (line.isEmpty()) {
				continue;
			}

			if (currentLine > previousLine) {// creates a link from the previous
				// to the current
				String from = lastNodePerLevel.get(lastNodePerLevel.size() - 1);
				this.addEdge(from, line);
			} else if (currentLine == previousLine) {// we're connected to the
				// factor at the
				// previous depth
				lastNodePerLevel.remove(lastNodePerLevel.get(lastNodePerLevel
						.size() - 1));// pops
				String from = lastNodePerLevel.get(lastNodePerLevel.size() - 1);
				this.addEdge(from, line);
			} else {// backtracking
				List<String> shortenedList = new LinkedList<String>();
				for (int i = 0; i < currentLine; i++) {
					shortenedList.add(lastNodePerLevel.get(i));
				}
				lastNodePerLevel = shortenedList;
				String from = lastNodePerLevel.get(lastNodePerLevel.size() - 1);
				this.addEdge(from, line);
			}
			lastNodePerLevel.add(line);// stacks
			previousLine = currentLine;
		}
	}

	// Added by Vishrant
	public void setBetweennessCentrality(
			BetweennessCentrality<Vertex, Edge> betweennessCentrality) {
		this.betweennessCentrality = betweennessCentrality;
	}

	public void setGraph(Graph<Vertex, Edge> graph) {
		this.graph = graph;
	}
}
