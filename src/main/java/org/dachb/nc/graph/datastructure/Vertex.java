/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.graph.datastructure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Vishrant Gupta
 */
public class Vertex implements GraphElement, Comparable<Vertex>, Cloneable {
	final int id;
	HashMap<String, Object> attributes;

	private Double score;

	public Vertex(int id) {
		this.attributes = new HashMap<String, Object>(3);
		this.id = id;
	}

	public Vertex(int id, String key, Object value) {
		this.attributes = new HashMap<String, Object>(3);
		this.id = id;
		this.attributes.put(key, value);
	}

	public void addAttribute(String key, Object value) {
		this.attributes.put(key, value);
		if (key.equals("FULLNAME")) {// Synchronizes short names with long names
			String nodeName = (String) value;
			if (nodeName.length() > 10)
				this.attributes.put("SHORTNAME", nodeName.substring(0, 10)
						+ "..");
			else
				this.attributes.put("SHORTNAME", nodeName);
		}
	}

	// Added by P. Giabbanelli in order to perform a deep copy of the graph
	@Override
	protected Object clone() {
		Vertex v = new Vertex(id);
		Iterator<String> keys = attributes.keySet().iterator();
		while (keys.hasNext()) {
			String tmp = keys.next();
			v.addAttribute(tmp, attributes.get(tmp));
		}
		return v;
	}

	// Added by P. Giabbanelli for graph models in which we had to distinguish
	// between generic vertices.
	@Override
	public int compareTo(Vertex v) {
		return v.getId() - this.getId();
	}

	// Added by P. Giabbanelli. Copy all specified attributes of the given node.
	public void copyAttributes(HashSet<String> whichAttributes, Vertex v) {
		for (String s : whichAttributes)
			// we assume that |whichAttributes|<|attributes(v)|
			if (v.attributes.keySet().contains(s))
				attributes.put(s, v.getAttribute(s));
	}

	// Copies ALL attributes from the given vertex
	public void copyAttributes(Vertex v) {
		for (String s : v.getKeys())
			attributes.put(s, v.getAttribute(s));
	}

	// Added by P. Giabbanelli. Temporary information are sometimes stored by
	// an algorithm at a Vertex and we prefer to erase it once the computation
	// is done in order to prevent the user from seeing all the crap we forgot.
	public void dropAttribute(String key) {
		attributes.remove(key);
	}

	// added by Issam in order to verify vertice equality.
	public boolean equals(Vertex v) {
		return (this.getId() == v.getId());
	}

	public Object getAttribute(String key) {
		if (key.equals("ID"))
			return getId();
		if (this.attributes.containsKey(key))
			return this.attributes.get(key);
		return null;
	}

	public String getFullName() {
		return (String) attributes.get("FULLNAME");
	}

	public int getId() {
		return this.id;
	}

	// Added by P. Giabbanelli; used in the TableHolder to initialize the
	// headers (columns)
	public String[] getKeys() {
		String[] res = new String[attributes.keySet().size() + 1];
		Iterator<String> it = attributes.keySet().iterator();
		int i = 1;
		res[0] = "ID";
		while (it.hasNext()) {
			res[i] = it.next();
			i++;
		}
		return res;
	}

	// alias for the function getFullName
	public String getName() {
		return getFullName();
	}

	public Double getNumber(String key) {
		if (this.attributes.containsKey(key))
			return (Double) this.attributes.get(key);
		else
			return 0.0;
	}

	public Double getScore() {
		return score;
	}

	public String getShortName() {
		return (String) attributes.get("SHORTNAME");
	}

	@Override
	public String getString(String key) {
		if (this.attributes.containsKey(key))
			return this.attributes.get(key).toString();
		else
			return this.toString();
	}

	// Giabbanelli. Since we added it for edges, may as well add it for
	// vertices.
	public boolean hasAttribute(String key) {
		return attributes.get(key) != null;
	}

	public void setFullName(String s) {
		attributes.put("FULLNAME", s);
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public void setShortName(String s) {
		attributes.put("SHORTNAME", s);
	}

	@Override
	public String toString() {
		return getName();
		// return "V"+this.id;
	}
}
