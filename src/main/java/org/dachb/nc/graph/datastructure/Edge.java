/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.graph.datastructure;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Vishrant Gupta
 */
public class Edge implements GraphElement, Comparable<Edge>, Cloneable {
	final int id;
	HashMap<String, Object> attributes;
	String comparableField;

	private Double score;

	public Edge(int id) {
		this.attributes = new HashMap<String, Object>(3);
		this.id = id;
		this.addAttribute("ID", id);
	}

	public Edge(int id, String key, Object value) {
		this(id);
		this.attributes.put(key, value);
	}

	public void addAttribute(String key, Object value) {
		this.attributes.put(key, value);
	}

	// Added by P. Giabbanelli in order to perform a deep copy of the graph
	@Override
	protected Object clone() {
		Edge e = new Edge(id);
		Iterator<String> keys = attributes.keySet().iterator();
		while (keys.hasNext()) {
			String tmp = keys.next();
			e.addAttribute(tmp, attributes.get(tmp));
		}
		e.comparableField = comparableField;
		return e;
	}

	@Override
	public int compareTo(Edge o) {
		if (this.comparableField != null)
			return (int) (this.getNumber(this.comparableField) - o
					.getNumber(this.comparableField));
		else
			return this.id - o.getId();
	}

	// Added by P. Giabbanelli; see motivations in Vertex.java
	public void dropAttribute(String key) {
		attributes.remove(key);
	}

	public Object getAttribute(String key) {
		if (this.attributes.containsKey(key))
			return this.attributes.get(key);
		else
			return null;
	}

	public int getId() {
		return this.id;
	}

	// Added by P. Giabbanelli; used in the TableHolder to initialize the
	// headers (columns)
	public String[] getKeys() {
		String[] res = new String[attributes.keySet().size() + 1];
		Iterator<String> it = attributes.keySet().iterator();
		int i = 1;
		res[0] = "ID";
		while (it.hasNext()) {
			res[i] = it.next();
			if (res[i].equals("ID"))
				continue;
			i++;
		}
		return res;
	}

	public Double getNumber(String key) {
		if (this.attributes.containsKey(key))
			return new Double("" + this.attributes.get(key));
		else
			return -1.0;
	}

	public Double getScore() {
		return score;
	}

	@Override
	public String getString(String key) {
		if (this.attributes.containsKey(key))
			return "" + (this.getNumber(key));
		else
			return this.toString();
	}

	// Giabbanelli. Short notations. Used in heuristics of
	// subtil.algorithms.heuristics
	public boolean hasAttribute(String key) {
		return attributes.get(key) != null;
	}

	public void setComparableField(String field) {
		this.comparableField = field;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "E" + this.id;
	}
}
