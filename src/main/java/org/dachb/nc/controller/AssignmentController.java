/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dachb.nc.common.AppConstant;
import org.dachb.nc.config.GsonUtility;
import org.dachb.nc.graph.GraphReader;
import org.dachb.nc.graph.datastructure.Edge;
import org.dachb.nc.graph.datastructure.EdgeDetails;
import org.dachb.nc.graph.datastructure.Vertex;
import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.Assignment;
import org.dachb.nc.model.AssignmentDetail;
import org.dachb.nc.model.AssignmentFile;
import org.dachb.nc.model.AssignmentMap;
import org.dachb.nc.model.InvitedUser;
import org.dachb.nc.model.SharedBean;
import org.dachb.nc.model.UpdateAssignmentSharing;
import org.dachb.nc.security.controller.TokenUtils;
import org.dachb.nc.service.AppUserService;
import org.dachb.nc.service.AssignmentService;
import org.dachb.nc.service.FileService;
import org.dachb.nc.validation.annotations.CheckObject;
import org.dachb.nc.validation.annotations.Validated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.uci.ics.jung.algorithms.scoring.BetweennessCentrality;
import edu.uci.ics.jung.graph.Graph;

@RestController
@RequestMapping(value = "rest")
@PreAuthorize("hasAnyRole('ROLE_USER')")
/**
 *
 * @author Vishrant Gupta
 */
public class AssignmentController {

	@Autowired
	AssignmentService assignmentService;

	@Autowired
	private FileService fileService;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private GsonUtility gson;

	@Autowired
	private AppUserService appUserService;

	@RequestMapping(value = "/file/deleteAssignmentMaps", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> deleteAssignmentMaps(
			@CheckObject @RequestBody AssignmentFile assignmentFile,
			final HttpServletRequest request) {

		if (assignmentFile != null) {

			try {

				final String token = request
						.getHeader(AppConstant.TOKEN_HEADER);
				final String username = this.tokenUtils
						.getUsernameFromToken(token);

				assignmentService.deleteAssignmentFileById(assignmentFile,
						username);

				return ResponseEntity.ok("success");

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(
				"Error");

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/file/downloadStudentMaps", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> downloadAssignmentStudentMaps(
			final HttpServletRequest request, HttpServletResponse response,
			@RequestBody final AssignmentFile assignmentFile) {

		HttpHeaders responseHeaders = new HttpHeaders();

		try {

			final String token = request.getHeader(AppConstant.TOKEN_HEADER);
			final String username = this.tokenUtils.getUsernameFromToken(token);

			AssignmentFile assignmentFileDetails = this.assignmentService
					.getAssignmentFileDetails(assignmentFile.getFileId(),
							username);

			if (assignmentFileDetails != null) {
				return ResponseEntity.ok(assignmentFileDetails
						.getJsonFileData());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity(null, responseHeaders,
				HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@RequestMapping(value = "getAssignmentDetail", method = RequestMethod.POST)
	public ResponseEntity<?> getAssignmentDetails(
			final HttpServletRequest request,
			@CheckObject @RequestBody final Assignment assignment) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		final Assignment assignmentDetails = this.assignmentService
				.getAssignmentDetails(assignment.getId(), username);

		return ResponseEntity.ok(assignmentDetails != null ? gson
				.toJson(assignmentDetails) : null);
	}

	@RequestMapping(value = "getAssignmentSharingDetail", method = RequestMethod.POST)
	public ResponseEntity<?> getAssignmentSharingDetails(
			final HttpServletRequest request,
			@CheckObject @RequestBody final Assignment assignment) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		final Assignment assignmentDetails = this.assignmentService
				.getAssignmentDetails(assignment.getId(), username);

		List<SharedBean> sharedWith = null;
		List<String> sharing = new ArrayList<String>();

		if (assignmentDetails != null) {

			sharedWith = assignmentDetails.getSharedWith();

			if (sharedWith != null) {

				for (SharedBean sharedBean : sharedWith) {

					AppUser appUser = appUserService
							.get(sharedBean.getUserId());

					if (appUser != null) {
						sharing.add(appUser.getEmailid());
					} else {
						InvitedUser invitedUser = appUserService
								.getInvitedUserByUserId(sharedBean.getUserId());
						sharing.add(invitedUser.getEmailId());
					}
				}

			}
		}

		return ResponseEntity.ok(sharing.size() > 0 ? gson.toJson(sharing)
				: null);
	}

	@RequestMapping(value = "getAssignmentsSharedWithMe", method = RequestMethod.POST)
	public ResponseEntity<?> getAssignmentsSharedWithAUser(
			final HttpServletRequest request) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		final List<Assignment> assignments = this.assignmentService
				.getAllAssignmentSharedWithAUser(username);

		List<AssignmentDetail> details = new LinkedList<>();
		for (Assignment assignment : assignments) {

			AssignmentDetail assignmentDetail = new AssignmentDetail();
			assignmentDetail.setId(assignment.getId());
			assignmentDetail.setAssignmentName(assignment.getAssignmentName());
			assignmentDetail.setClassName(assignment.getClassName());
			assignmentDetail.setDueDate(assignment.getDueDate());
			assignmentDetail.setGradeLevel(assignment.getGradeLevel());
			assignmentDetail.setNote(assignment.getNote());
			assignmentDetail.setNumberOfPoints(assignment.getNumberOfPoints());

			AppUser appUser = appUserService.get(assignment.getOwner());
			String email = "";

			if (appUser == null) {
				InvitedUser invitedUser = appUserService
						.getInvitedUserByUserId(assignment.getOwner());
				email = invitedUser.getEmailId();
			} else {
				email = appUser.getEmailid();
			}

			assignmentDetail.setOwner(email);

			details.add(assignmentDetail);
		}

		return ResponseEntity.ok(gson.toJson(details));
	}

	@RequestMapping(value = "getAllMyAssignments", method = RequestMethod.POST)
	public ResponseEntity<?> getUserAssignment(final HttpServletRequest request) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		final List<Assignment> assignments = this.assignmentService
				.getAllUserAssignment(username);

		List<AssignmentDetail> details = new LinkedList<>();
		for (Assignment assignment : assignments) {

			AssignmentDetail assignmentDetail = new AssignmentDetail();
			assignmentDetail.setId(assignment.getId());
			assignmentDetail.setAssignmentName(assignment.getAssignmentName());
			assignmentDetail.setClassName(assignment.getClassName());
			assignmentDetail.setDueDate(assignment.getDueDate());
			assignmentDetail.setGradeLevel(assignment.getGradeLevel());
			assignmentDetail.setNote(assignment.getNote());
			assignmentDetail.setNumberOfPoints(assignment.getNumberOfPoints());

			AppUser appUser = appUserService.get(assignment.getOwner());
			String email = "";

			if (appUser == null) {
				InvitedUser invitedUser = appUserService
						.getInvitedUserByUserId(assignment.getOwner());
				email = invitedUser.getEmailId();
			} else {
				email = appUser.getEmailid();
			}

			assignmentDetail.setOwner(email);

			details.add(assignmentDetail);
		}

		return ResponseEntity.ok(gson.toJson(details));

	}

	@RequestMapping(value = "addAssignment", method = RequestMethod.POST)
	@Validated
	public ResponseEntity<?> newAssignment(
			@CheckObject @RequestBody final Assignment assignment,
			final HttpServletRequest request) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		final Map<String, String> msg = this.assignmentService.save(assignment,
				username);

		if ((msg != null) && (msg.size() > 0)) {

			final StringBuilder response = new StringBuilder();

			for (final Entry<String, String> entry : msg.entrySet()) {
				response.append(entry.getValue() + "\n");
			}

			return ResponseEntity.ok(response.toString());
		} else {
			return ResponseEntity.ok("success");
		}

	}

	@RequestMapping(value = "removeAssignment", method = RequestMethod.POST)
	public ResponseEntity<?> removeAssignment(final HttpServletRequest request,
			@CheckObject @RequestBody final Assignment assignment) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		AppUser appUser = appUserService.loadUserByUsername(username);

		boolean removed = assignmentService.removeAssignment(assignment,
				appUser);

		return ResponseEntity.ok(removed ? "success" : "error");
	}

	// private static final Gson gson = new Gson();

	@RequestMapping(value = "unshareAssignment", method = RequestMethod.POST)
	public ResponseEntity<?> unshareAssignment(
			final HttpServletRequest request,
			@CheckObject @RequestBody final UpdateAssignmentSharing updateAssignmentSharing) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		AppUser appUser = appUserService.loadUserByUsername(username);

		boolean removed = assignmentService.unshareAssignment(
				updateAssignmentSharing.getAssignment(), appUser,
				updateAssignmentSharing.getRemovedEmailId());

		return ResponseEntity.ok(removed ? "success" : "error");
	}

	@RequestMapping(value = "/file/uploadStudentMaps", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> uploadAssignmentStudentMaps(
			@CheckObject @RequestBody AssignmentMap assignmentMap,
			final HttpServletRequest request) {

		if (assignmentMap != null) {

			try {
				final AssignmentFile assignmentFile = new AssignmentFile();
				assignmentFile.setAssignmentId(assignmentMap.getAssignmentId());
				assignmentFile.setFileName(assignmentMap.getFilename());
				assignmentFile.setGraphType(assignmentMap.getGraphType());

				org.dachb.nc.graph.datastructure.Graph graph = gson.fromJson(
						assignmentMap.getGraphData(),
						org.dachb.nc.graph.datastructure.Graph.class);

				GraphReader graphReader = new GraphReader();
				Collection<EdgeDetails> edgeDetails = graph.getEdges();

				for (EdgeDetails edge : edgeDetails) {
					graphReader.addEdge(edge.getSource(), edge.getTarget());
				}

				Graph<Vertex, Edge> g = graphReader.getGraph();

				BetweennessCentrality<Vertex, Edge> centrality = new BetweennessCentrality<Vertex, Edge>(
						g);
				graphReader.setBetweennessCentrality(centrality);

				assignmentFile.setJsonFileData(graphReader
						.getGraphInJSONFormat());

				this.fileService.saveAssignmentFile(assignmentFile);

				return ResponseEntity.ok(/* gson.toJson(assignment) */"success");

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(
				"Error");

	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> uploadMap(final HttpServletRequest request) {

		return null;
	}

}
