/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.controller;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.dachb.nc.common.AppConstant;
import org.dachb.nc.config.GsonUtility;
import org.dachb.nc.model.Alignment;
import org.dachb.nc.model.AlignmentEntity;
import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.UserAlignment;
import org.dachb.nc.recommendation.neo4j.domain.AlignedWith;
import org.dachb.nc.recommendation.neo4j.domain.Term;
import org.dachb.nc.recommendation.neo4j.service.RecommendationService;
import org.dachb.nc.security.controller.TokenUtils;
import org.dachb.nc.service.AppUserService;
import org.dachb.nc.service.UserAlignmentService;
import org.dachb.nc.validation.annotations.CheckObject;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
@RequestMapping(value = "rest")
@PreAuthorize("hasAnyRole('ROLE_USER')")
/**
 *
 * @author Vishrant Gupta
 */
public class AlignmentController {

	public final class Test {
	}

	@Autowired
	private SessionFactory neo4jSessionFactory;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private AppUserService appUserService;

	@Autowired
	private RecommendationService recommendationService;

	@Autowired
	private GsonUtility gson;

	@Autowired
	private UserAlignmentService rdbmsUserAlignmentService;

	@RequestMapping(value = "submitAlignment", method = RequestMethod.POST)
	public ResponseEntity<?> getAssignmentDetails(
			final HttpServletRequest request,
			@CheckObject @RequestBody final UserAlignment userAlignment) {

		if (userAlignment != null) {

			final String token = request.getHeader(AppConstant.TOKEN_HEADER);
			final String username = this.tokenUtils.getUsernameFromToken(token);

			AppUser appUser = appUserService.loadUserByUsername(username);

			Session session = neo4jSessionFactory.openSession();

			Set<String> terms = userAlignment.getTerms();
			Set<Alignment> termAlignment = userAlignment.getAlignment();

			session.beginTransaction();

			// Gson gson = new Gson();

			AlignmentEntity alignment = rdbmsUserAlignmentService.getAlignment(
					userAlignment, appUser.getId());
			Set<Alignment> existingAlignment = new HashSet<Alignment>();
			if (alignment != null) {

				Type listType = new TypeToken<HashSet<Alignment>>() {
				}.getType();
				Set<Alignment> alignments = gson.fromJson(
						alignment.getAlignmentInJson(), listType);

				existingAlignment.addAll(alignments);

			}

			save(appUser, session, terms, termAlignment, existingAlignment);

			session.getTransaction().commit();
			String alignmentInJson = gson.toJson(termAlignment);

			if (alignment == null) {
				alignment = new AlignmentEntity();
			}

			alignment.setAlignmentInJson(alignmentInJson);
			alignment.setAssignmentId(userAlignment.getAssignmentId());
			alignment.setExpertMapId(userAlignment.getExpertMapId());
			alignment.setStudentMapId(userAlignment.getStudentMapId());
			alignment.setUserId(appUser.getId());

			rdbmsUserAlignmentService.save(alignment);

		}

		return ResponseEntity.ok("success");

	}

	@RequestMapping(value = "getAlignment", method = RequestMethod.POST)
	public ResponseEntity<?> getUserAlignmentDetails(
			final HttpServletRequest request,
			@CheckObject @RequestBody final UserAlignment userAlignment) {

		final String token = request.getHeader(AppConstant.TOKEN_HEADER);
		final String username = this.tokenUtils.getUsernameFromToken(token);

		AppUser appUser = appUserService.loadUserByUsername(username);

		AlignmentEntity alignment = rdbmsUserAlignmentService.getAlignment(
				userAlignment, appUser.getId());

		return ResponseEntity.ok(gson.toJson(alignment));
	}

	public void save(AppUser appUser, Session session, Set<String> terms,
			Set<Alignment> termAlignment/* Map<String, String> termsMapping */,
			Set<Alignment> userExistingAlignment) {
		// getting existing nodes
		Collection<Term> existingTerms = session.loadAll(Term.class);
		Map<Term, Term> mapTerms = new HashMap<>();
		existingTerms.stream().forEach(i -> mapTerms.put(i, i));

		for (String termName : terms) {
			Term term = new Term();
			term.setTermName(termName);

			boolean exists = mapTerms.containsKey(term);
			if (!exists) {
				session.save(term);
			}
		}

		existingTerms = session.loadAll(Term.class);
		mapTerms.clear();
		existingTerms.stream().forEach(i -> mapTerms.put(i, i));

		// getting existing relationships/ term alignment
		Collection<AlignedWith> existingAlignment = session
				.loadAll(AlignedWith.class);
		Map<AlignedWith, AlignedWith> mapAlignment = new HashMap<>();
		existingAlignment.stream().forEach(i -> mapAlignment.put(i, i));

		userExistingAlignment.removeAll(existingAlignment);

		for (Alignment alignment : termAlignment) {

			// repeated submission: start and end node is same
			if (userExistingAlignment.contains(alignment)) {
				continue;
			}

			userExistingAlignment.forEach(previousAlignment -> {
				if (previousAlignment.getStartNode().equalsIgnoreCase(
						alignment.getStartNode())) {
					if (!previousAlignment.getEndNode().equalsIgnoreCase(
							alignment.getEndNode())) {

						AlignedWith aw = new AlignedWith();

						Term s = new Term();
						s.setTermName(previousAlignment.getStartNode());

						Term e = new Term();
						e.setTermName(previousAlignment.getEndNode());

						// replacing with exiting student term
						if (mapTerms.containsKey(s)) {
							s = mapTerms.get(s);
						}

						// replacing with exiting expert term
						if (mapTerms.containsKey(e)) {
							e = mapTerms.get(e);
						}

						aw.setStartTerm(s);
						aw.setEndTerm(e);

						if (mapAlignment.containsKey(aw)) {
							aw = mapAlignment.get(aw);
							// alignedWith.setAll(mapAlignment.get(alignedWith));
						}

						aw.addDeptEndorsement(appUser.getClassName(), -1D);

						Map<String, Double> deptEndorsement = aw
								.getDeptEndorsement();

						boolean ifSave = false;
						for (Entry<String, Double> entry : deptEndorsement
								.entrySet()) {
							if (entry.getValue().compareTo(0D) > 0) {
								ifSave = true;
							}
						}

						if (ifSave) {
							session.save(aw);
						} else {
							session.delete(aw);
						}

						// userExistingAlignment.remove(previousAlignment);
					}
				}
			})	;

			Term student = new Term();
			student.setTermName(alignment.getStartNode());

			Term expert = new Term();
			expert.setTermName(alignment.getEndNode());

			// replacing with exiting student term
			boolean existsStudent = mapTerms.containsKey(student);
			if (existsStudent) {
				student = mapTerms.get(student);
			}

			// replacing with exiting expert term
			boolean existsExpert = mapTerms.containsKey(expert);
			if (existsExpert) {
				expert = mapTerms.get(expert);
			}

			AlignedWith alignedWith = new AlignedWith();

			alignedWith.setStartTerm(student);
			alignedWith.setEndTerm(expert);

			boolean existsAlignment = mapAlignment.containsKey(alignedWith);
			if (existsAlignment) {
				alignedWith = mapAlignment.get(alignedWith);
			}

			alignedWith.addDeptEndorsement(appUser.getClassName(), 1D);

			session.save(alignedWith);
		}
	}

}
