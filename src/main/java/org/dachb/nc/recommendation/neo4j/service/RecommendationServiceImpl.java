/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.recommendation.neo4j.service;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dachb.nc.config.GsonUtility;
import org.dachb.nc.graph.datastructure.VertexDetail;
import org.dachb.nc.model.Alignment;
import org.dachb.nc.model.AlignmentEntity;
import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.Assignment;
import org.dachb.nc.model.AssignmentFile;
import org.dachb.nc.model.GraphType;
import org.dachb.nc.model.Recommendation;
import org.dachb.nc.model.UserAlignment;
import org.dachb.nc.recommendation.neo4j.domain.AlignedWith;
import org.dachb.nc.recommendation.neo4j.domain.Term;
import org.dachb.nc.service.AssignmentService;
import org.dachb.nc.service.UserAlignmentService;
import org.neo4j.ogm.cypher.ComparisonOperator;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.reflect.TypeToken;

//import com.google.gson.Gson;

@Service("recommendationService")
/**
 *
 * @author Vishrant Gupta
 */
public class RecommendationServiceImpl implements RecommendationService {

	private static double ALPHA_VALUE = 1.5D;

	@Autowired
	private SessionFactory neo4jSessionFactory;

	@Autowired
	private AssignmentService assignmentService;

	// private Gson gson = new Gson();
	@Autowired
	private GsonUtility gson;

	@Autowired
	private UserAlignmentService rdbmsUserAlignmentService;

	@Override
	public Set<Recommendation> getRecommendationForTerm(
			org.dachb.nc.model.Term termName, AppUser appUser) {

		try {
			if (termName != null) {

				Assignment assignment = assignmentService.getAssignmentDetails(
						termName.getAssignmentId(), appUser.getUsername());

				List<AssignmentFile> assignmentFiles = assignment
						.getAssignmentFiles();

				org.dachb.nc.graph.datastructure.Graph graph = null;

				for (AssignmentFile assignmentFile : assignmentFiles) {
					if (assignmentFile.getGraphType().compareTo(
							GraphType.EXPERT) == 0) {
						graph = gson.fromJson(assignmentFile.getJsonFileData(),
								org.dachb.nc.graph.datastructure.Graph.class);
						break;
					}
				}

				if (graph == null) {
					return null;
				}

				Collection<VertexDetail> nodes = graph.getNodes();

				Map<String, String> recommendationNodes = new HashMap<>();
				nodes.stream().forEach(
						i -> recommendationNodes.put(i.getName(), i.getName()));

				Session session = neo4jSessionFactory.openSession();

				session.beginTransaction();

				Filter filter = new Filter("termName",
						ComparisonOperator.EQUALS, termName.getTermName());
				Collection<Term> terms = session.loadAll(Term.class, filter);

				if (terms != null && terms.size() > 0) {

					List<AlignedWith> alignedWith = new LinkedList<AlignedWith>();
					Set<Recommendation> recommendations = new HashSet<>();

					terms.forEach(term -> {
						alignedWith.addAll(term.getAlignedWith());
					});

					float total = 0;

					UserAlignment userAlignment = new UserAlignment();
					userAlignment.setAssignmentId(termName.getAssignmentId());
					userAlignment.setStudentMapId(termName.getStudentMapId());
					userAlignment.setExpertMapId(termName.getExpertMapId());

					AlignmentEntity alignmentEntity = rdbmsUserAlignmentService
							.getAlignment(userAlignment, appUser.getId());

					Set<Alignment> alignments = null;

					if (alignmentEntity != null) {

						String alignmentForThisUser = alignmentEntity
								.getAlignmentInJson();

						Type listType = new TypeToken<HashSet<Alignment>>() {
						}.getType();
						alignments = gson.fromJson(alignmentForThisUser,
								listType);

					}

					for (AlignedWith alignment : alignedWith) {
						if (recommendationNodes.containsKey(alignment
								.getEndTerm().getTermName())) {
							if (alignment.getDeptEndorsement().get(
									appUser.getClassName()) != null) {

								if (alignments != null
										&& alignments.contains(new Alignment(
												alignment.getStartTerm()
												.getTermName(),
												alignment.getEndTerm()
												.getTermName()))) {
									// user already endorsed this alignment
									// reduce it by 1
									alignment
									.changeDeptEndorsement(
											appUser.getClassName(),
											new Double(
													alignment
													.getDeptEndorsement()
													.get(appUser
															.getClassName()) - 1)
											* ALPHA_VALUE);
								} else {
									alignment
									.changeDeptEndorsement(
											appUser.getClassName(),
											new Double(
													alignment
													.getDeptEndorsement()
													.get(appUser
															.getClassName())
															* ALPHA_VALUE));
								}
							}
							total += alignment.getSumOfEndorsement();
						}/*
						 * else { System.out.println("Does not contains " +
						 * recommendationNodes.containsKey(alignment
						 * .getEndTerm().getTermName())); }
						 */
					}

					if (total > 0) {
						for (AlignedWith alignment : alignedWith) {

							if (recommendationNodes.containsKey(alignment
									.getEndTerm().getTermName())) {
								Recommendation r = new Recommendation();
								r.setTermName(alignment.getEndTerm()
										.getTermName());
								r.setPercentage(alignment.getSumOfEndorsement()
										/ total * 100f);

								recommendations.add(r);
							}/*
							 * else { System.out.println("Skipping " +
							 * recommendationNodes.containsKey(alignment
							 * .getEndTerm().getTermName())); }
							 */
						}
					}

					return recommendations;

				}/*
				 * else { return null; }
				 */
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
