/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.recommendation.neo4j.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;
//import org.neo4j.ogm.annotation.Properties;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@RelationshipEntity(type = "ALIGNED_WITH")
/**
 *
 * @author Vishrant Gupta
 */
public class AlignedWith {

	// private Set<DeptEndorsement> deptEndorsement = new HashSet<>();
	// private Set<String> deptEndorsement = new HashSet<>();
	@Properties
	private Map<String, Double> deptEndorsement = new HashMap<>();

	@EndNode
	private Term endTerm;

	// @GraphId
	@Id
	@GeneratedValue
	private Long id;

	@StartNode
	private Term startTerm;

	public AlignedWith() {
		super();
	}

	public void addDeptEndorsement(final String dept, final Double count) {

		if (this.deptEndorsement.containsKey(dept)) {
			final Double c = this.deptEndorsement.get(dept);
			this.deptEndorsement.put(dept, c + count);
		} else {
			this.deptEndorsement.put(dept, count);
		}

	}

	public void changeDeptEndorsement(final String dept, final Double count) {
		deptEndorsement.put(dept, count);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final AlignedWith other = (AlignedWith) obj;
		if (this.endTerm == null) {
			if (other.endTerm != null) {
				return false;
			}
		} else if (!this.endTerm.equals(other.endTerm)) {
			return false;
		}
		if (this.startTerm == null) {
			if (other.startTerm != null) {
				return false;
			}
		} else if (!this.startTerm.equals(other.startTerm)) {
			return false;
		}
		return true;
	}

	public Map<String, Double> getDeptEndorsement() {
		return this.deptEndorsement;
	}

	public Term getEndTerm() {
		return this.endTerm;
	}

	public Long getId() {
		return this.id;
	}

	public Term getStartTerm() {
		return this.startTerm;
	}

	public Float getSumOfEndorsement() {

		float total = 0F;

		for (Entry<String, Double> entry : deptEndorsement.entrySet()) {
			total += entry.getValue();
		}

		return total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result)
				+ ((this.endTerm == null) ? 0 : this.endTerm.hashCode());
		result = (prime * result)
				+ ((this.startTerm == null) ? 0 : this.startTerm.hashCode());
		return result;
	}

	public boolean isCyclic() {
		return startTerm.getTermName().equalsIgnoreCase(endTerm.getTermName());
	}

	public void setAll(AlignedWith alignedWith) {
		this.deptEndorsement = alignedWith.getDeptEndorsement();
		this.endTerm = alignedWith.getEndTerm();
		this.id = alignedWith.getId();
		this.startTerm = alignedWith.getStartTerm();
	}

	public void setDeptEndorsement(final Map<String, Double> deptEndorsement) {
		this.deptEndorsement = deptEndorsement;
	}

	// @Override
	// public String toString() {
	// return "AlignedWith [id=" + id + ", startTerm=" + startTerm
	// + ", endTerm=" + endTerm + "]";
	// }

	public void setEndTerm(final Term endTerm) {
		this.endTerm = endTerm;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setStartTerm(final Term startTerm) {
		this.startTerm = startTerm;
	}

	@Override
	public String toString() {
		return "AlignedWith [deptEndorsement=" + this.deptEndorsement
				+ ", endTerm=" + this.endTerm.getTermName() + ", id=" + this.id
				+ ", startTerm=" + this.startTerm.getTermName() + "]";
	}

}
