/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
//package org.dachb.nc.recommendation.neo4j.controller;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.dachb.nc.model.Assignment;
//import org.dachb.nc.recommendation.neo4j.service.MovieService;
//import org.dachb.nc.validation.annotations.CheckObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.google.gson.Gson;
//
//@RestController
//@RequestMapping(value = "rest")
//public class AssignmentDetailsController {
//
//	@Autowired
//	private MovieService movieService;
//
//	@RequestMapping(value = "getAssignmentDetail", method = RequestMethod.POST)
//	public ResponseEntity<?> getAssignmentDetails(
//			final HttpServletRequest request,
//			@CheckObject @RequestBody final Assignment assignment) {
//
//		final Gson gson = new Gson();
//		final Map<String, Object> graph = new HashMap<>();
//
//		new Thread() {
//
//			public void run() {
//				System.out.println("movieService Vishrant is null " + movieService);
//
//				Map<String, Object> graph2 = movieService.graph(10);
//				graph.putAll(graph2);
//
//				System.out.println("Graph " + graph);
//			};
//
//		}.start();
//		return ResponseEntity.ok(graph != null ? gson.toJson(graph) : null);
//
//		// new Thread() {
//		//
//		// public void run() {
//		//
//		// if(movieService != null) {
//		// System.out.println("Movie service Vishrant -->> " +
//		// movieService.graph(10));
//		// }
//		//
//		// }
//		//
//		// }.start();
//
//	}
// }
