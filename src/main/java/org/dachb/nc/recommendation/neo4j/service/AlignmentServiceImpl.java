/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.recommendation.neo4j.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.dachb.nc.model.AppUser;
import org.dachb.nc.recommendation.neo4j.domain.AlignedWith;
import org.dachb.nc.recommendation.neo4j.domain.Term;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("alignmentService")
/**
 *
 * @author Vishrant Gupta
 */
public class AlignmentServiceImpl implements AlignmentService {

	@Autowired
	private SessionFactory neo4jSessionFactory;

	@Autowired
	private RecommendationService recommendationService;

	@Override
	public void save(AppUser appUser, Set<String> terms,
			Map<String, String> termsMapping) {

		Session session = neo4jSessionFactory.openSession();

		// getting existing nodes
		Collection<Term> existingTerms = session.loadAll(Term.class);
		Map<Term, Term> mapTerms = new HashMap<>();
		existingTerms.stream().forEach(i -> mapTerms.put(i, i));

		String t = null;

		for (String termName : terms) {
			Term term = new Term();
			term.setTermName(termName);

			t = termName;

			boolean exists = mapTerms.containsKey(term);
			if (!exists) {
				session.save(term);
			}
		}

		existingTerms = session.loadAll(Term.class);
		mapTerms.clear();
		existingTerms.stream().forEach(i -> mapTerms.put(i, i));

		// getting existing relationships/ term alignment
		Collection<AlignedWith> existingAlignment = session
				.loadAll(AlignedWith.class);
		Map<AlignedWith, AlignedWith> mapAlignment = new HashMap<>();
		existingAlignment.stream().forEach(i -> mapAlignment.put(i, i));

		// recommendationService.getRecommendationForTerm(t);

		Term term = new Term();
		term.setTermName(t);

		// Long id = session.resolveGraphIdFor(term);

		for (Entry<String, String> entry : termsMapping.entrySet()) {

			Term student = new Term();
			student.setTermName(entry.getKey());

			boolean existsStudent = mapTerms.containsKey(student);
			if (existsStudent) {
				student = mapTerms.get(student);
			}

			Term expert = new Term();
			expert.setTermName(entry.getValue());

			boolean existsExpert = mapTerms.containsKey(expert);
			if (existsExpert) {
				expert = mapTerms.get(expert);
			}

			// Check if alignment already exists between two nodes
			// if yes, then get the alignment, and increase the count by 1
			// if no, then create new alignment

			AlignedWith alignedWith = new AlignedWith();

			alignedWith.setStartTerm(student);
			alignedWith.setEndTerm(expert);

			// cycle detected
			// if(alignedWith.isCyclic()) {
			// continue;
			// }

			boolean existsAlignment = mapAlignment.containsKey(alignedWith);
			if (existsAlignment) {
				alignedWith = mapAlignment.get(alignedWith);
			}

			alignedWith.addDeptEndorsement(appUser.getClassName(), 1D);

			session.save(alignedWith);

			session.getTransaction().commit();
		}
	}

}
