/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import java.sql.Date;
import java.util.List;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.dachb.nc.common.AppConstant;
//import org.dachb.nc.validation.annotations.CheckRegexp;
import org.dachb.nc.validation.annotations.CheckRegexp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "assignment")
/**
 *
 * @author Vishrant Gupta
 */
public class Assignment {

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "assignment_id")
	private List<AssignmentFile> assignmentFiles;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "assignment_name")
	private String assignmentName;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "class_name")
	private String className;

	@Column(name = "create_date")
	private Date createDate = new Date(System.currentTimeMillis());

	// @CheckRegexp(expression = AppConstant.DATE_VALIDATION_REGEX)
	@NotNull
	@Column(name = "due_date")
	private Date dueDate;

	@CheckRegexp(expression = AppConstant.ALPHA_NUMERIC_REGEX)
	@Column(name = "grade_level")
	private String gradeLevel;

	// if variable is made transient then Gson will not be able to set graphType
	// from JSON String instead use @Transient
	@Transient
	private String graphType;

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "assignment_id")
	// @Exclude // to exclude from mapping it to "null" as not required,
	// hibernate will generate its own
	private Long id;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "note")
	private String note;

	@Column(name = "num_of_points", columnDefinition = "FLOAT")
	private String numberOfPoints;

	@Column(name = "owner")
	private Long owner;

	@OneToMany(fetch = FetchType.EAGER/* , cascade = CascadeType.REMOVE */)
	// @Cascade(CascadeType.DELETE)
	@JoinColumn(name = "assignment_id")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<SharedBean> sharedWith;

	@CheckRegexp(expression = AppConstant.EMAIL_VALIDATION_REGEX)
	// @Column(name = "assignment_name")
	private transient List<String> sharingEmail;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Assignment other = (Assignment) obj;
		if (this.assignmentName == null) {
			if (other.assignmentName != null) {
				return false;
			}
		} else if (!this.assignmentName.equals(other.assignmentName)) {
			return false;
		}
		if (this.className == null) {
			if (other.className != null) {
				return false;
			}
		} else if (!this.className.equals(other.className)) {
			return false;
		}
		if (this.gradeLevel == null) {
			if (other.gradeLevel != null) {
				return false;
			}
		} else if (!this.gradeLevel.equals(other.gradeLevel)) {
			return false;
		}
		if (this.graphType == null) {
			if (other.graphType != null) {
				return false;
			}
		} else if (!this.graphType.equals(other.graphType)) {
			return false;
		}
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		if (this.numberOfPoints == null) {
			if (other.numberOfPoints != null) {
				return false;
			}
		} else if (!this.numberOfPoints.equals(other.numberOfPoints)) {
			return false;
		}
		return true;
	}

	public List<AssignmentFile> getAssignmentFiles() {
		return this.assignmentFiles;
	}

	public String getAssignmentName() {
		return this.assignmentName;
	}

	public String getClassName() {
		return this.className;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public Date getDueDate() {
		return this.dueDate;
	}

	public String getGradeLevel() {
		return this.gradeLevel;
	}

	public String getGraphType() {
		return this.graphType;
	}

	public Long getId() {
		return this.id;
	}

	public String getNote() {
		return this.note;
	}

	public String getNumberOfPoints() {
		return this.numberOfPoints;
	}

	public Long getOwner() {
		return this.owner;
	}

	public List<SharedBean> getSharedWith() {
		return this.sharedWith;
	}

	public List<String> getSharingEmail() {
		return this.sharingEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((this.assignmentName == null) ? 0 : this.assignmentName
						.hashCode());
		result = prime * result
				+ ((this.className == null) ? 0 : this.className.hashCode());
		result = prime * result
				+ ((this.gradeLevel == null) ? 0 : this.gradeLevel.hashCode());
		result = prime * result
				+ ((this.graphType == null) ? 0 : this.graphType.hashCode());
		result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
		result = prime
				* result
				+ ((this.numberOfPoints == null) ? 0 : this.numberOfPoints
						.hashCode());
		return result;
	}

	public void setAssignmentFiles(final List<AssignmentFile> assignmentFiles) {
		this.assignmentFiles = assignmentFiles;
	}

	public void setAssignmentName(final String assignmentName) {
		this.assignmentName = assignmentName;
	}

	public void setClassName(final String className) {
		this.className = className;
	}

	public void setCreateDate(final Date createDate) {
		this.createDate = createDate;
	}

	public void setDueDate(final Date dueDate) {
		this.dueDate = dueDate;
	}

	public void setGradeLevel(final String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public void setGraphType(final String graphType) {
		this.graphType = graphType;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public void setNumberOfPoints(final String numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}

	public void setOwner(final Long owner) {
		this.owner = owner;
	}

	public void setSharedWith(final List<SharedBean> sharedWith) {
		this.sharedWith = sharedWith;
	}

	public void setSharingEmail(final List<String> sharingEmail) {
		this.sharingEmail = sharingEmail;
	}

	@Override
	public String toString() {
		return "Assignment [assignmentFiles=" + this.assignmentFiles
				+ ", assignmentName=" + this.assignmentName + ", className="
				+ this.className + ", createDate=" + this.createDate
				+ ", dueDate=" + this.dueDate + ", gradeLevel="
				+ this.gradeLevel + ", graphType=" + this.graphType + ", id="
				+ this.id + ", note=" + this.note + ", numberOfPoints="
				+ this.numberOfPoints + ", owner=" + this.owner
				+ ", sharedWith=" + this.sharedWith + "]";
	}

}
