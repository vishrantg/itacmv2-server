/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.dachb.nc.common.AppConstant;
import org.dachb.nc.validation.annotations.CheckRegexp;

@Entity
@Table(name = "user_alignment")
/**
 *
 * @author Vishrant Gupta
 */
public class AlignmentEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "id")
	private Long id;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "user_id")
	private Long userId;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "assignment_id")
	private Long assignmentId;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "student_map_id")
	private Long studentMapId;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "expert_map_id")
	private Long expertMapId;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "alignment")
	private String alignmentInJson;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlignmentEntity other = (AlignmentEntity) obj;
		if (assignmentId == null) {
			if (other.assignmentId != null)
				return false;
		} else if (!assignmentId.equals(other.assignmentId))
			return false;
		if (expertMapId == null) {
			if (other.expertMapId != null)
				return false;
		} else if (!expertMapId.equals(other.expertMapId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (studentMapId == null) {
			if (other.studentMapId != null)
				return false;
		} else if (!studentMapId.equals(other.studentMapId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public String getAlignmentInJson() {
		return alignmentInJson;
	}

	public Long getAssignmentId() {
		return assignmentId;
	}

	public Long getExpertMapId() {
		return expertMapId;
	}

	public Long getId() {
		return id;
	}

	public Long getStudentMapId() {
		return studentMapId;
	}

	public Long getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assignmentId == null) ? 0 : assignmentId.hashCode());
		result = prime * result
				+ ((expertMapId == null) ? 0 : expertMapId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((studentMapId == null) ? 0 : studentMapId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	public void setAlignmentInJson(String alignmentInJson) {
		this.alignmentInJson = alignmentInJson;
	}

	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public void setExpertMapId(Long expertMapId) {
		this.expertMapId = expertMapId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStudentMapId(Long studentMapId) {
		this.studentMapId = studentMapId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "AlignmentEntity [id=" + id + ", userId=" + userId
				+ ", assignmentId=" + assignmentId + ", studentMapId="
				+ studentMapId + ", expertMapId=" + expertMapId + "]";
	}

}
