/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import java.util.Set;

/**
 *
 * @author Vishrant Gupta
 */
public class UserAlignment {

	private Set<Alignment> alignment;
	private Set<String> terms;

	private Long assignmentId;
	private Long studentMapId;
	private Long expertMapId;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAlignment other = (UserAlignment) obj;
		if (assignmentId == null) {
			if (other.assignmentId != null)
				return false;
		} else if (!assignmentId.equals(other.assignmentId))
			return false;
		if (expertMapId == null) {
			if (other.expertMapId != null)
				return false;
		} else if (!expertMapId.equals(other.expertMapId))
			return false;
		if (studentMapId == null) {
			if (other.studentMapId != null)
				return false;
		} else if (!studentMapId.equals(other.studentMapId))
			return false;
		return true;
	}

	public Set<Alignment> getAlignment() {
		return alignment;
	}

	public Long getAssignmentId() {
		return assignmentId;
	}

	public Long getExpertMapId() {
		return expertMapId;
	}

	public Long getStudentMapId() {
		return studentMapId;
	}

	public Set<String> getTerms() {
		return terms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assignmentId == null) ? 0 : assignmentId.hashCode());
		result = prime * result
				+ ((expertMapId == null) ? 0 : expertMapId.hashCode());
		result = prime * result
				+ ((studentMapId == null) ? 0 : studentMapId.hashCode());
		return result;
	}

	public void setAlignment(Set<Alignment> alignment) {
		this.alignment = alignment;
	}

	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public void setExpertMapId(Long expertMapId) {
		this.expertMapId = expertMapId;
	}

	public void setStudentMapId(Long studentMapId) {
		this.studentMapId = studentMapId;
	}

	public void setTerms(Set<String> terms) {
		this.terms = terms;
	}

	@Override
	public String toString() {
		return "UserAlignment [alignment=" + alignment + ", terms=" + terms
				+ ", assignmentId=" + assignmentId + ", studentMapId="
				+ studentMapId + ", expertMapId=" + expertMapId + "]";
	}

}
