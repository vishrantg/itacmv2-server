/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.dachb.nc.common.AppConstant;
import org.dachb.nc.validation.annotations.CheckRegexp;

@Entity
@Table(name = "files")
/**
 *
 * @author Vishrant Gupta
 */
public class AssignmentFile {

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "assignment_id")
	private Long assignmentId;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	// @Cascade(CascadeType.DELETE)
	@Column(name = "file_id")
	private Long fileId;

	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	@Column(name = "file_name")
	private String fileName;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "graph_type")
	private GraphType graphType;

	@Column(name = "json_file_data")
	private String jsonFileData;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssignmentFile other = (AssignmentFile) obj;
		if (assignmentId == null) {
			if (other.assignmentId != null)
				return false;
		} else if (!assignmentId.equals(other.assignmentId))
			return false;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		} else if (!fileId.equals(other.fileId))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (graphType != other.graphType)
			return false;
		return true;
	}

	public Long getAssignmentId() {
		return this.assignmentId;
	}

	public Long getFileId() {
		return this.fileId;
	}

	public String getFileName() {
		return this.fileName;
	}

	public GraphType getGraphType() {
		return this.graphType;
	}

	public String getJsonFileData() {
		return jsonFileData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assignmentId == null) ? 0 : assignmentId.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result
				+ ((graphType == null) ? 0 : graphType.hashCode());
		return result;
	}

	public void setAssignmentId(final Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public void setFileId(final Long fileId) {
		this.fileId = fileId;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	public void setGraphType(final String graphType) {

		if (null != graphType) {

			if (GraphType.EXPERT.getType().equalsIgnoreCase(graphType)) {
				this.graphType = GraphType.EXPERT;
			} else if (GraphType.STUDENT.getType().equalsIgnoreCase(graphType)) {
				this.graphType = GraphType.STUDENT;
			}

		}

	}

	public void setJsonFileData(String jsonFileData) {
		this.jsonFileData = jsonFileData;
	}

	@Override
	public String toString() {
		return "AssignmentFile [assignmentId=" + assignmentId + ", fileId="
				+ fileId + ", fileName=" + fileName + ", graphType="
				+ graphType + "]";
	}

}
