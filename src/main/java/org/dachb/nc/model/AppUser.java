/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.dachb.nc.common.AppConstant;
import org.dachb.nc.validation.annotations.CheckRegexp;

@Entity
@Table(name = "appuser")
/**
 *
 * @author Vishrant Gupta
 */
public class AppUser {

	// @Column(name = "authorities")
	private String authorities = AppConstant.APP_USER_ROLE;

	@Column(name = "email_id")
	@CheckRegexp(expression = AppConstant.EMAIL_VALIDATION_REGEX)
	private String emailid;

	@Column(name = "firstname")
	@CheckRegexp(expression = AppConstant.ALPHA_NUMERIC_REGEX)
	private String firstName;

	@Column(name = "grade_level")
	@CheckRegexp(expression = AppConstant.ALPHA_NUMERIC_REGEX)
	private String gradeLevel;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// Here the same generator will be shared with InvitedUser
	/*
	 * If there is a new app user a value will be generated let say 1 now a user
	 * is invited then the value will be 2 instead of 1
	 */
	@TableGenerator(name = "next_user_id")
	@Column(name = "id")
	private Long id;

	@Column(name = "lastname")
	@CheckRegexp(expression = AppConstant.ALPHA_NUMERIC_REGEX)
	private String lastName;

	@Column(name = "password", nullable = false)
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	private String password;

	@Column(name = "school_name")
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	private String schoolName;

	@Column(name = "class_name")
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	private String className;

	@Column(name = "username", unique = true, nullable = false)
	@CheckRegexp(expression = AppConstant.TEXT_VALIDATION_REGEX)
	private String username;

	public AppUser() {
		setAuthorities(AppConstant.APP_USER_ROLE);
	}

	public AppUser(String username, String password, String authorities) {
		this();

		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public String getAuthorities() {
		return authorities;
	}

	public String getClassName() {
		return className;
	}

	public String getEmailid() {
		return emailid;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getGradeLevel() {
		return gradeLevel;
	}

	public Long getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPassword() {
		return password;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public String getUsername() {
		return username;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setGradeLevel(String gradeLevel) {
		this.gradeLevel = gradeLevel;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "AppUser [authorities=" + authorities + ", emailid=" + emailid
				+ ", firstName=" + firstName + ", gradeLevel=" + gradeLevel
				+ ", id=" + id + ", lastName=" + lastName + ", schoolName="
				+ schoolName + ", className=" + className + ", username="
				+ username + "]";
	}
}
