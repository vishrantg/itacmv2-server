/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.model;

import java.util.Objects;

/**
 *
 * @author Vishrant
 */
/**
 *
 * @author Vishrant Gupta
 */
public class AssignmentMap {

	private Long assignmentId;
	private String graphType;
	private String graphData;
	private String filename;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AssignmentMap other = (AssignmentMap) obj;
		if (!Objects.equals(this.graphType, other.graphType)) {
			return false;
		}
		if (!Objects.equals(this.graphData, other.graphData)) {
			return false;
		}
		if (!Objects.equals(this.assignmentId, other.assignmentId)) {
			return false;
		}
		return true;
	}

	public Long getAssignmentId() {
		return assignmentId;
	}

	public String getFilename() {
		return filename;
	}

	public String getGraphData() {
		return graphData;
	}

	public String getGraphType() {
		return graphType;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 17 * hash + Objects.hashCode(this.assignmentId);
		hash = 17 * hash + Objects.hashCode(this.graphType);
		hash = 17 * hash + Objects.hashCode(this.graphData);
		return hash;
	}

	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setGraphData(String graphData) {
		this.graphData = graphData;
	}

	public void setGraphType(String graphType) {
		this.graphType = graphType;
	}

	@Override
	public String toString() {
		return "AssignmentMap [assignmentId=" + assignmentId + ", graphType="
				+ graphType + ", graphData=" + graphData + ", filename="
				+ filename + "]";
	}

}
