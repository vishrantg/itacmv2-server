/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.Assignment;
import org.dachb.nc.model.AssignmentFile;
import org.dachb.nc.model.InvitedUser;
import org.dachb.nc.model.SharedBean;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "assignmentService")
@Transactional(transactionManager = "transactionManager", rollbackFor = Exception.class)
/**
 *
 * @author Vishrant Gupta
 */
public class AssignmentServiceImpl implements AssignmentService {

	@Autowired
	private AppUserService appUserService;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SharedService sharedService;

	@Override
	public boolean deleteAssignmentFileById(AssignmentFile assignmentFile,
			String username) {
		this.sessionFactory.getCurrentSession().delete(assignmentFile);
		this.sessionFactory.getCurrentSession().flush();
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Assignment> getAllAssignmentSharedWithAUser(String username) {

		AppUser appUser = this.appUserService.loadUserByUsername(username);

		Long userId = appUser.getId();

		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(SharedBean.class);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<SharedBean> sharedEmail = criteria.add(
				Restrictions.eq("userId", userId)).list();

		List<Assignment> assignments = new ArrayList<Assignment>();

		for (SharedBean sharedBean : sharedEmail) {

			criteria = this.sessionFactory.getCurrentSession().createCriteria(
					Assignment.class);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

			List<Assignment> assignmentList = criteria.add(
					Restrictions.eq("id", sharedBean.getAssignmentId())).list();
			assignments.addAll(assignmentList);
		}

		return assignments;
	}

	@Override
	@SuppressWarnings("unchecked")
	// @Transactional(rollbackFor = Exception.class)
	public List<Assignment> getAllUserAssignment(String username) {

		AppUser appUser = this.appUserService.loadUserByUsername(username);

		Long userId = appUser.getId();

		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(Assignment.class);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Assignment> result = criteria
				.add(Restrictions.eq("owner", userId)).list();

		return result;

	}

	@Override
	@SuppressWarnings("unchecked")
	// @Transactional(rollbackFor = Exception.class)
	public Assignment getAssignmentDetails(Long id, String username) {

		AppUser appUser = this.appUserService.loadUserByUsername(username);

		Long userId = appUser.getId();

		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(Assignment.class, "assignment");
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<Assignment> result = criteria.add(Restrictions.eq("id", id))
				.list();

		if (result != null && result.size() > 0) {
			Assignment assignment = result.get(0);

			// assignment.getOwner() == userId INCORRECT WAY OF COMPARING LONG
			// OBJECT
			// BECAUSE OF LONG CACHING ANY VALUE BELOW 127 WILL WORK FINE NOT
			// ABOVE IT
			if (assignment.getOwner().compareTo(userId) == 0) {
				return assignment;
			}

			List<SharedBean> sharedWith = assignment.getSharedWith();
			for (SharedBean shared : sharedWith) {
				if (shared.getUserId().compareTo(userId) == 0) {
					return assignment;
				}
			}
		}

		return /* result != null && result.size() > 0 ? result.get(0) : */null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AssignmentFile getAssignmentFileDetails(Long fileId, String username) {

		AppUser appUser = this.appUserService.loadUserByUsername(username);

		Long userId = appUser.getId();

		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(AssignmentFile.class);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		List<AssignmentFile> result = criteria.add(
				Restrictions.eq("fileId", fileId)).list();

		if (result != null) {
			for (AssignmentFile assignmentFile : result) {

				criteria = this.sessionFactory.getCurrentSession()
						.createCriteria(Assignment.class);
				criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

				List<Assignment> assignments = criteria
						.add(Restrictions.eq("id",
								assignmentFile.getAssignmentId())).list();

				if (assignments != null && assignments.size() > 0) {
					for (Assignment assignment : assignments) {

						if (assignment.getOwner().compareTo(userId) == 0) {
							return assignmentFile;
						}

						List<SharedBean> sharedWith = assignment
								.getSharedWith();

						if (sharedWith != null) {
							for (SharedBean sharedBean : sharedWith) {
								if (sharedBean.getUserId().compareTo(userId) == 0) {
									return assignmentFile;
								}
							}
						}
					}
				}

			}
		}

		return /* result != null && result.size() > 0 ? result.get(0) : */null;
	}

	@Override
	public boolean removeAssignment(Assignment assignment, AppUser appUser) {

		Assignment a = this.sessionFactory.getCurrentSession().load(
				Assignment.class, assignment.getId());

		if (a != null
				&& new Long(a.getOwner()).compareTo(new Long(appUser.getId())) == 0) {

			for (SharedBean shared : a.getSharedWith()) {
				this.sessionFactory.getCurrentSession().delete(shared);
			}

			for (AssignmentFile file : a.getAssignmentFiles()) {
				this.sessionFactory.getCurrentSession().delete(file);
			}

			sessionFactory.getCurrentSession().flush();

			Query query = this.sessionFactory
					.getCurrentSession()
					.createSQLQuery(
							"DELETE FROM assignment WHERE assignment_id = :assignmentId");

			query.setParameter("assignmentId", a.getId());

			query.executeUpdate();

			return true;
		}

		return false;
	}

	// @Transactional(rollbackFor = Exception.class)
	@Override
	public Map<String, String> save(Assignment assignment, String username) {

		Map<String, String> msg = new HashMap<String, String>();

		AppUser appUser = this.appUserService.loadUserByUsername(username);

		assignment.setOwner(appUser.getId());

		List<String> emails = assignment.getSharingEmail();
		List<String> removeEmails = new ArrayList<String>();

		if (emails != null) {
			for (String email : emails) {
				AppUser user = this.appUserService.loadUserByEmailId(email);

				if (user != null
						&& user.getUsername().equalsIgnoreCase(username)) {
					removeEmails.add(email);

					msg.put(email,
							"You do not need to share this assignment with yourself. You already have access to it.");
					return msg;
				}
			}
			emails.removeAll(removeEmails);
		}

		// if (emails.size() > 0) {
		Long assignmentId = assignment.getId();
		if (assignmentId == null) {
			assignmentId = (Long) this.sessionFactory.getCurrentSession().save(
					assignment);
		} else {

			Query query = this.sessionFactory
					.getCurrentSession()
					.createSQLQuery(
							"UPDATE assignment "
									+ "SET assignment_name=:assignmentName, grade_level=:gradeLevel, "
									+ "num_of_points=:numofPoints,class_name=:className, "
									+ "due_date=:dueDate,note=:note WHERE assignment_id = :assignmentId");

			query.setParameter("assignmentName", assignment.getAssignmentName());
			query.setParameter("gradeLevel", assignment.getGradeLevel());
			query.setParameter("numofPoints", assignment.getNumberOfPoints());
			query.setParameter("className", assignment.getClassName());
			query.setParameter("dueDate", assignment.getDueDate());
			query.setParameter("note", assignment.getNote());
			query.setParameter("assignmentId", assignment.getId());

			query.executeUpdate();

		}

		if (emails != null) {
			for (String email : emails) {
				AppUser user = this.appUserService.loadUserByEmailId(email);

				SharedBean sharedBean = new SharedBean();
				sharedBean.setAssignmentId(assignmentId);

				if (user != null) {
					sharedBean.setUserId(user.getId());
				} else {

					InvitedUser invitedUsers = this.appUserService
							.loadInvitedUsersByEmailId(email);

					Long saveInvitedUser = null;
					if (invitedUsers == null) {

						InvitedUser invitedUser = new InvitedUser();
						invitedUser.setEmailId(email);

						saveInvitedUser = this.appUserService
								.saveInvitedUser(invitedUser);
					} else {
						saveInvitedUser = invitedUsers.getId();
					}

					sharedBean.setUserId(saveInvitedUser);

					// USER NOT RESISRETED
					// msg.put(email, "User with " + email
					// + " does not exists, an invitation email is sent. Once he
					// register he can access the shared assignments");
				}

				this.sharedService.save(sharedBean);
			}
		}
		// }

		return msg;
	}

	@Override
	public boolean unshareAssignment(Assignment assignment, AppUser appUser,
			String removedEmailId) {

		Assignment a = this.sessionFactory.getCurrentSession().load(
				Assignment.class, assignment.getId());

		if (a != null
				&& new Long(a.getOwner()).compareTo(new Long(appUser.getId())) == 0) {

			Long userId = Long.MIN_VALUE;

			AppUser removedUser = appUserService
					.loadUserByEmailId(removedEmailId);
			if (removedUser == null) {
				InvitedUser invitedUser = appUserService
						.loadInvitedUsersByEmailId(removedEmailId);

				if (invitedUser != null) {
					userId = invitedUser.getId();
				}
			} else {
				userId = removedUser.getId();
			}

			if (!userId.equals(Long.MIN_VALUE)) {
				for (SharedBean shared : a.getSharedWith()) {
					if (shared.getUserId().equals(userId)) {
						this.sessionFactory.getCurrentSession().delete(shared);
					}
				}
			}

			return true;
		}

		return false;
	}
}
