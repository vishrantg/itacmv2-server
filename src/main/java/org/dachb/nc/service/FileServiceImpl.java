/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.dachb.nc.model.AssignmentFile;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service(value = "fileService")
@Transactional(transactionManager = "transactionManager", rollbackFor = Exception.class)
/**
 *
 * @author Vishrant Gupta
 */
public class FileServiceImpl implements FileService {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<String> deleteFiles(final List<String> files) {

		final List<String> deletedFiles = new ArrayList<>();

		for (final String path : files) {
			final File f = new File(path);
			f.delete();

			if (!f.exists()) {
				deletedFiles.add(path);
			}
		}

		return deletedFiles;
	}

	@Override
	public boolean saveAssignmentFile(final AssignmentFile assignmentFiles) {

		this.sessionFactory.getCurrentSession().saveOrUpdate(assignmentFiles);
		this.sessionFactory.getCurrentSession().flush();

		return true;
	}

	@Override
	public boolean saveFile(final byte[] fileContent,
			final String absoluteFileName) {

		try {

			final File file = new File(absoluteFileName);

			final BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(file));

			AssignmentFile assignmentFile = new AssignmentFile();
			assignmentFile.setJsonFileData(new String(fileContent));

			stream.write(fileContent);

			stream.flush();
			stream.close();

			return file.exists();

		} catch (final Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public List<String> saveMultiPartFiles(final List<MultipartFile> files,
			final String folder, final String graphType) {

		final List<String> savedFiles = new ArrayList<>();

		try {
			for (final MultipartFile multipartFile : files) {

				final byte[] bytes = multipartFile.getBytes();

				final String name = multipartFile.getOriginalFilename();
				this.saveFile(bytes, name);

				savedFiles.add(name);
			}

			return savedFiles;
		} catch (final Exception e) {
			this.deleteFiles(savedFiles);
			e.printStackTrace();
		}

		return null;
	}

}
