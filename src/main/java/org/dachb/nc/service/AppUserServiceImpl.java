/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.service;

import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.InvitedUser;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "appUserService")
@Transactional("transactionManager")
/**
 *
 * @author Vishrant Gupta
 */
public class AppUserServiceImpl implements AppUserService {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean delete(long id) {
		sessionFactory.getCurrentSession().delete(get(id));
		return true;
	}

	@Override
	public AppUser get(long id) {
		return sessionFactory.getCurrentSession().get(AppUser.class, id);
	}

	@Override
	public InvitedUser getInvitedUserByUserId(Long userId) {
		return sessionFactory.getCurrentSession()
				.get(InvitedUser.class, userId);
	}

	@Override
	public InvitedUser loadInvitedUsersByEmailId(String emailId) {
		return (InvitedUser) sessionFactory.getCurrentSession()
				.createCriteria(InvitedUser.class)
				.add(Restrictions.eq("emailId", emailId)).uniqueResult();
	}

	@Override
	public AppUser loadUserByEmailId(String emailId) {
		return (AppUser) sessionFactory.getCurrentSession()
				.createCriteria(AppUser.class)
				.add(Restrictions.eq("emailid", emailId)).uniqueResult();
	}

	// @Transactional
	@Override
	public AppUser loadUserByUsername(String username) {
		return (AppUser) sessionFactory.getCurrentSession()
				.createCriteria(AppUser.class)
				.add(Restrictions.eq("username", username)).uniqueResult();
	}

	@Override
	public AppUser patch(AppUser appUser) {
		sessionFactory.getCurrentSession().update(appUser);
		return get(appUser.getId());
	}

	@Override
	public long post(AppUser appUser) {

		if (appUser.getId() != null) {
			Query query = sessionFactory
					.getCurrentSession()
					.createSQLQuery(
							"INSERT INTO appuser(password, username, id, firstname, lastname, school_name, grade_level, class_name, email_id, authorities) "
									+ "	VALUES (:password, :username, :id, :firstname, :lastname, :school_name, :grade_level, :class_name, :email_id, :authorities)");
			query.setParameter("password", appUser.getPassword());
			query.setParameter("username", appUser.getUsername());
			query.setParameter("id", appUser.getId());
			query.setParameter("firstname", appUser.getFirstName());
			query.setParameter("lastname", appUser.getLastName());
			query.setParameter("school_name", appUser.getSchoolName());
			query.setParameter("grade_level", appUser.getGradeLevel());
			query.setParameter("class_name", appUser.getClassName());
			query.setParameter("email_id", appUser.getEmailid());
			query.setParameter("authorities", appUser.getAuthorities());

			return query.executeUpdate();
		} else {
			Long l = (Long) sessionFactory.getCurrentSession().save(appUser);
			return l.longValue();
		}
	}

	@Override
	public Long saveInvitedUser(InvitedUser invitedUser) {
		Long l = (Long) sessionFactory.getCurrentSession().save(invitedUser);
		return l.longValue();
	}

}
