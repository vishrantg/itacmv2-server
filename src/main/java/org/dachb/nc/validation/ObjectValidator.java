/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.validation;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

/**
 *
 * @author Vishrant Gupta
 */
public class ObjectValidator implements IVerifier {

	@Override
	public void validate(Object param, Annotation annotation,
			Annotation methodAnnotation) throws CustomException {
		Class<?> beanClass = param.getClass();
		Field[] fields = beanClass.getDeclaredFields();
		Map<Class<?>, Class<? extends IVerifier>> verifiersMap = ArgumentsValidator
				.getVerifiersMap();

		for (Field field : fields) {
			Annotation[] fieldAnnotations = field.getAnnotations();
			for (Annotation fieldAnnotation : fieldAnnotations) {
				if (verifiersMap.containsKey(fieldAnnotation.annotationType())) {
					Class<? extends IVerifier> verifierClass = verifiersMap
							.get(fieldAnnotation.annotationType());
					IVerifier verifier = null;

					try {
						verifier = verifierClass.newInstance();
					} catch (Exception exception) {
						exception.printStackTrace();
						throw new CustomException(
								"can't create varifier instance");
					}

					Object value = null;

					try {
						value = new PropertyDescriptor(field.getName(),
								beanClass).getReadMethod().invoke(param);
					} catch (Exception exception) {
						exception.printStackTrace();
						throw new CustomException(
								"can't create property descriptor");
					}

					verifier.validate(value, fieldAnnotation, methodAnnotation);
				}
			}
		}
	}
}
