/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.dachb.nc.validation.annotations.CheckObject;
import org.dachb.nc.validation.annotations.CheckRegexp;
import org.dachb.nc.validation.annotations.Validated;

/**
 *
 * @author Vishrant Gupta
 */
public class ArgumentsValidator {

	private static Map<Class<?>, Class<? extends IVerifier>> verifiersMap = null;

	static {
		verifiersMap = new HashMap<Class<?>, Class<? extends IVerifier>>();
		verifiersMap.put(CheckRegexp.class, RegExpValidator.class);
		verifiersMap.put(CheckObject.class, ObjectValidator.class);
	}

	private static void execValueValidator(Object value, Annotation annotation,
			Annotation methodAnnotation) throws CustomException {
		try {
			Class<? extends IVerifier> verifierClass = verifiersMap
					.get(annotation.annotationType());

			if (verifierClass != null) {
				IVerifier curVerifier = verifierClass.newInstance();
				curVerifier.validate(value, annotation, methodAnnotation);
			}
		} catch (IllegalAccessException exception) {
			exception.printStackTrace();
		} catch (InstantiationException exception) {
			exception.printStackTrace();
		} catch (CustomException exception) {
			throw exception;
		}
	}

	public static Map<Class<?>, Class<? extends IVerifier>> getVerifiersMap() {
		return verifiersMap;
	}

	public static void validate(Method method, Object[] args)
			throws CustomException {
		Annotation[][] paramsAnnotations = method.getParameterAnnotations();
		Annotation[] methodAnotation = method.getAnnotations();
		Annotation mAnnotation = null;

		for (Annotation annotation : methodAnotation) {
			if (annotation.annotationType() == Validated.class) {
				mAnnotation = annotation;
				break;
			}
		}

		for (int iArg = 0; iArg < args.length; iArg++) {
			Annotation[] curParamAnnotations = paramsAnnotations[iArg];
			for (Annotation annotation : curParamAnnotations) {
				execValueValidator(args[iArg], annotation, mAnnotation);
			}
		}
	}
}
