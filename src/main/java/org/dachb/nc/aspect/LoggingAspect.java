/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.aspect;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Logger for application, this is applied around a web method and when an
 * exception occured
 *
 * */

@Aspect
@Component
/**
 *
 * @author Vishrant Gupta
 */
public class LoggingAspect {
	private static Logger LOGGER = LoggerFactory.getLogger("LOGGER");

	//
	// @AfterThrowing(pointcut =
	// "execution(* org.dachb.nc.controller..*.*(..)) || execution(* org.dachb.nc.service..*.*(..)) || execution(* org.dachb.nc.recommendation..*.*(..)) || execution(* org.dachb.nc.graph..*.*(..))",
	// throwing = "excep")
	// public void afterThrowing(JoinPoint pjp, Throwable excep) throws
	// Throwable {
	//
	// String packageName = pjp.getSignature().getDeclaringTypeName();
	// String methodName = pjp.getSignature().getName();
	//
	// // if (!pjp.getSignature().getName().equals("initBinder")) {
	// //
	// // StringWriter sw = new StringWriter();
	// // excep.printStackTrace(new PrintWriter(sw));
	// //
	// // String exceptionAsString = sw.toString();
	// //
	// //
	// // }
	//
	// LOGGER.error(packageName + "." + methodName + " " + excep.getMessage()
	// + "\n" + excep + " >> " + excep.getMessage() + "]");
	//
	// }

	//
	@Around("execution(* org.dachb.nc.controller..*.*(..)) || execution(* org.dachb.nc.service..*.*(..)) || execution(* org.dachb.nc.recommendation..*.*(..)) || execution(* org.dachb.nc.graph..*.*(..))")
	public Object aroundWebMethodE(ProceedingJoinPoint pjp) throws Throwable {
		String packageName = pjp.getSignature().getDeclaringTypeName();
		String methodName = pjp.getSignature().getName();
		long start = System.currentTimeMillis();
		if (!pjp.getSignature().getName().equals("initBinder")) {
			LOGGER.info("Entering method [" + packageName + "." + methodName
					+ "] " + " arguments passed "
					+ Arrays.toString(pjp.getArgs()));
		}
		Object output = pjp.proceed();
		long elapsedTime = System.currentTimeMillis() - start;
		if (!methodName.equals("initBinder")) {
			LOGGER.info("Exiting method [" + packageName + "." + methodName
					+ "]; exec time (ms): " + elapsedTime);
		}
		return output;
	}

}
