/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("applicationProperties")
/**
 *
 * @author Vishrant Gupta
 */
public class ApplicationProperties {

	@Value("${spring.data.neo4j.uri}")
	private String neo4jURI;

	@Value("${spring.data.neo4j.username}")
	private String neo4jUsername;

	@Value("${spring.data.neo4j.password}")
	private String neo4jPassword;

	@Value("${mysql.databaseName}")
	private String rdbmsDatabase;

	@Value("${mysql.port}")
	private String rdbmsPort;

	@Value("${mysql.serverName}")
	private String rdbmsServer;

	@Value("${mysql.user}")
	private String rdbmsUser;

	@Value("${mysql.password}")
	private String rdbmsPassword;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateHbm2ddlAuto;

	@Value("${hibernate.show_sql}")
	private Boolean showSql;

	@Value("${hibernate.datasource}")
	private String hibernateDatasource;

	public String getHibernateDatasource() {
		return hibernateDatasource;
	}

	public String getHibernateDialect() {
		return hibernateDialect;
	}

	public String getHibernateHbm2ddlAuto() {
		return hibernateHbm2ddlAuto;
	}

	public String getNeo4jPassword() {
		return neo4jPassword;
	}

	public String getNeo4jURI() {
		return neo4jURI;
	}

	public String getNeo4jUsername() {
		return neo4jUsername;
	}

	public String getRdbmsDatabase() {
		return rdbmsDatabase;
	}

	public String getRdbmsPassword() {
		return rdbmsPassword;
	}

	public String getRdbmsPort() {
		return rdbmsPort;
	}

	public String getRdbmsServer() {
		return rdbmsServer;
	}

	public String getRdbmsUser() {
		return rdbmsUser;
	}

	public Boolean getShowSql() {
		return showSql;
	}

	public void setHibernateDatasource(String hibernateDatasource) {
		this.hibernateDatasource = hibernateDatasource;
	}

	public void setHibernateDialect(String hibernateDialect) {
		this.hibernateDialect = hibernateDialect;
	}

	public void setHibernateHbm2ddlAuto(String hibernateHbm2ddlAuto) {
		this.hibernateHbm2ddlAuto = hibernateHbm2ddlAuto;
	}

	public void setNeo4jPassword(String neo4jPassword) {
		this.neo4jPassword = neo4jPassword;
	}

	public void setNeo4jURI(String neo4jURI) {
		this.neo4jURI = neo4jURI;
	}

	public void setNeo4jUsername(String neo4jUsername) {
		this.neo4jUsername = neo4jUsername;
	}

	public void setRdbmsDatabase(String rdbmsDatabase) {
		this.rdbmsDatabase = rdbmsDatabase;
	}

	public void setRdbmsPassword(String rdbmsPassword) {
		this.rdbmsPassword = rdbmsPassword;
	}

	public void setRdbmsPort(String rdbmsPort) {
		this.rdbmsPort = rdbmsPort;
	}

	public void setRdbmsServer(String rdbmsServer) {
		this.rdbmsServer = rdbmsServer;
	}

	public void setRdbmsUser(String rdbmsUser) {
		this.rdbmsUser = rdbmsUser;
	}

	public void setShowSql(Boolean showSql) {
		this.showSql = showSql;
	}

	@Override
	public String toString() {
		return "ApplicationProperties [neo4jURI=" + neo4jURI
				+ ", rdbmsDatabase=" + rdbmsDatabase + ", rdbmsPort="
				+ rdbmsPort + ", rdbmsServer=" + rdbmsServer
				+ ", hibernateDialect=" + hibernateDialect
				+ ", hibernateHbm2ddlAuto=" + hibernateHbm2ddlAuto
				+ ", showSql=" + showSql + "]";
	}

}
