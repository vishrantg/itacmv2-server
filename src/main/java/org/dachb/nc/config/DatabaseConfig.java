/*******************************************************************************
 * Written by Vishrant K. Gupta <vishrant.gupta@gmail.com>, January 2018
 * 
 * This file is part of Vishrant K. Gupta thesis "Efficiently Comparing 
 * Mental Representations: Visualizing and Matching Causal Networks"
 * 
 * If you are using this application, please cite this paper: 
 * "An online environment to compare student's and expert solutions to 
 * ill-structured problems"
 * 
 * BSD 3-clause (Modified) License
 * 
 * Copyright (C) 2018, Vishrant K. Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
package org.dachb.nc.config;

import java.util.Properties;

import org.dachb.nc.model.AlignmentEntity;
import org.dachb.nc.model.AppUser;
import org.dachb.nc.model.Assignment;
import org.dachb.nc.model.AssignmentFile;
import org.dachb.nc.model.GraphType;
import org.dachb.nc.model.InvitedUser;
import org.dachb.nc.model.SharedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
/**
 *
 * @author Vishrant Gupta
 */
public class DatabaseConfig {

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private ApplicationProperties applicationProperties;

	@Bean
	public HikariDataSource getDataSource() {
		HikariDataSource dataSource = new HikariDataSource();

		dataSource.setDataSourceClassName(applicationProperties
				.getHibernateDatasource());
		dataSource.addDataSourceProperty("databaseName",
				applicationProperties.getRdbmsDatabase());
		dataSource.addDataSourceProperty("portNumber",
				applicationProperties.getRdbmsPort());
		dataSource.addDataSourceProperty("serverName",
				applicationProperties.getRdbmsServer());
		dataSource.addDataSourceProperty("user",
				applicationProperties.getRdbmsUser());
		dataSource.addDataSourceProperty("password",
				applicationProperties.getRdbmsPassword());

		return dataSource;
	}

	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean hibernate5SessionFactoryBean() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(appContext
				.getBean(HikariDataSource.class));
		localSessionFactoryBean.setAnnotatedClasses(AppUser.class,
				Assignment.class, SharedBean.class, InvitedUser.class,
				AssignmentFile.class, GraphType.class, AlignmentEntity.class);
		// localSessionFactoryBean.setAnnotatedClasses(Assignment.class);

		Properties properties = new Properties();

		// properties.put("hibernate.current_session_context_class","thread");
		// // because I am using Spring, it will take care of session context
		/*
		 *
		 * Spring will by default set its own CurrentSessionContext
		 * implementation (the SpringSessionContext), however if you set it
		 * yourself this will not be the case. Basically breaking proper
		 * transaction integration.
		 *
		 * Ref:
		 * https://stackoverflow.com/questions/18832889/spring-transactions-
		 * and-hibernate-current-session-context-class
		 */
		properties.put("hibernate.dialect",
				applicationProperties.getHibernateDialect());

		properties.put("hibernate.hbm2ddl.auto",
				applicationProperties.getHibernateHbm2ddlAuto());
		properties
		.put("hibernate.show_sql", applicationProperties.getShowSql());
		// properties.put("hibernate.hbm2ddl.import_files",
		// "/org/dachb/nc/resource/default_data.sql"); // this will execute only
		// when hbm2ddl.auto is set to "create" or "create-drop"
		// properties.put("connection.autocommit", "true");

		localSessionFactoryBean.setHibernateProperties(properties);
		return localSessionFactoryBean;
	}

	@Bean("transactionManager")
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager manager = new HibernateTransactionManager();
		manager.setSessionFactory(hibernate5SessionFactoryBean().getObject());
		return manager;
	}
}
